#cd Cube2/ && mv res.gif Cube.gif && cd ..
#cd Tetrahedron/ && mv res.gif Tetrahedron.gif && cd ..
#cd Dodecahedron/ && mv res.gif Dodecahedron.gif && cd ..
#cd Icosahedron/ && mv res.gif Icosahedron.gif && cd ..
#cd Octahedron/ && mv res.gif Octahedron.gif && cd ..
#cd J12_TriangularDipyramid/ && mv res.gif J12_TriangularDipyramid.gif && cd ..
#cd J13_PentagonalDipyramid/ && mv res.gif J13_PentagonalDipyramid.gif && cd ..
#cd J17_GyroelongatedSquareDipyramid/ && mv res.gif J17_GyroelongatedSquareDipyramid.gif && cd ..
#cd J51_TriaugmentedTriangularPrism/ && mv res.gif J51_TriaugmentedTriangularPrism.gif && #cd ..
#cd J84_SnubDisphenoid/ && mv res.gif J84_SnubDisphenoid.gif && cd ..
cd RhombicDodecahedron/ && mv res.gif RhombicDodecahedron.gif && cd ..
cd RhombicIcosahedron/ && mv res.gif RhombicIcosahedron.gif && cd ..
cd RhombicTriacontahedron/ && mv res.gif RhombicTriacontahedron.gif && cd ..
#cd / && mv res.gif .gif && cd ..

cd stell_Cube2/ && mv res.gif stell_Cube.gif && cd ..
cd stell_Tetrahedron/ && mv res.gif stell_Tetrahedron.gif && cd ..
cd stell_Dodecahedron/ && mv res.gif stell_Dodecahedron.gif && cd ..
cd stell_Icosahedron/ && mv res.gif stell_Icosahedron.gif && cd ..
cd stell_Octahedron/ && mv res.gif stell_Octahedron.gif && cd ..
cd stell_J12_TriangularDipyramid/ && mv res.gif stell_J12_TriangularDipyramid.gif && cd ..
cd stell_J13_PentagonalDipyramid/ && mv res.gif stell_J13_PentagonalDipyramid.gif && cd ..
cd stell_J17_GyroelongatedSquareDipyramid/ && mv res.gif stell_J17_GyroelongatedSquareDipyramid.gif && cd ..
cd stell_J51_TriaugmentedTriangularPrism/ && mv res.gif stell_J51_TriaugmentedTriangularPrism.gif && cd ..
cd stell_J84_SnubDisphenoid/ && mv res.gif stell_J84_SnubDisphenoid.gif && cd ..
cd stell_RhombicDodecahedron/ && mv res.gif stell_RhombicDodecahedron.gif && cd ..
cd stell_RhombicIcosahedron/ && mv res.gif stell_RhombicIcosahedron.gif && cd ..
cd stell_RhombicTriacontahedron/ && mv res.gif stell_RhombicTriacontahedron.gif && cd ..

echo ‘fini’
