import sys
import os
import glob
from subprocess import call
from collections import namedtuple
import click

# name is the filename
# frame is the number of consecutive frames, if set to -1 all frames with filename are added
Sequence= namedtuple('Sequence', ['name','frame'])

def pingpongseq(seqlist,nb):
    sequences=[]
    for i in range(nb):
        for el in seqlist:
            sequences.append(el)
    return sequences

def makeAnimation(sequences, outputfilename, param=None):
    if param is None:
        param={}
    if "resize" not in param.keys():
        param["resize"]= "100%"
    if "delay" not in param.keys():
        param["delay"]= "4"

    cwd = os.getcwd()
    cmdstr=[]
    count=0
    for el in sequences:
        elstr= os.path.join(cwd, el.name)
        # make list of files ending with _xxxx.png
        namelist= glob.glob(elstr + '_[0-9][0-9][0-9][0-9].png')
        namelist.sort()

        if el.frame == -1:
            st,ed=(0,len(namelist))
        else:
            st,ed= (count,count + el.frame)
            count= ed
        for fname in namelist[st:ed]:
            cmdstr.append(fname)

    cmdline= ["convert"]
    cmdline.extend(["-resize", param["resize"]])
    cmdline.extend(["-delay", param["delay"]])
    cmdline.extend(cmdstr)
    cmdline.extend(["-loop", "0"])
    cmdline.extend([outputfilename])
    call(cmdline)
    return None


def makeflags(defaultvalue=None):
    if defaultvalue is None:
        bval= False
    else:
        bval = defaultvalue
    return {'mixupbase': bval, 'mixupstell': bval, 'pingpong': bval, 'fullgif': bval}

def main(flags):

    # mixup animation of the base models
    basepolyname= ['Tetrahedron', 'Cube2', 'Octahedron',
    'Icosahedron', 'Dodecahedron', 'J12_TriangularDipyramid',
    'J13_PentagonalDipyramid', 'J17_GyroelongatedSquareDipyramid',
    'J51_TriaugmentedTriangularPrism', 'J84_SnubDisphenoid',
    'RhombicDodecahedron', 'RhombicIcosahedron', 'RhombicTriacontahedron',
    'RhombicHexecontahedron', 'BilinskiDodecahedron',
    'AcuteGoldenRhombohedron', 'ObtuseGoldenRhombohedron']

    atltcolpoly= ["RhombicHexecontahedron5col", "Dodecahedron6col",
    "J12_TriangularDipyramid3col", "J13_PentagonalDipyramid3col",
    "J17_GyroelongatedSquareDipyramid3col", "J51_TriaugmentedTriangularPrism3col",
    "J84_SnubDisphenoid3col"]

    temp=['RhombicTriacontahedron', 'RhombicHexecontahedron5col', 'J51_TriaugmentedTriangularPrism3col', 'RhombicIcosahedron_alt']

    temp2=["stell_Dodecahedron_sym", "stell_Icosahedron_5col",
    "stell_RhombicDodecahedron_5col", "stell_RhombicTriacontahedron_5col",
    "stell_BilinskiDodecahedron_5col"]

    basepolyseq=[]
    for el in basepolyname:
        basepolyseq.append(Sequence(os.path.join(el,el),1))
    if flags['mixupbase']:
        makeAnimation(basepolyseq, "mixup25.gif", param={"delay":"4", "resize":"50%"})

    # mixup animation of the stellated models
    stellpolyname= ['stell_Tetrahedron', 'stell_Cube2', 'stell_Octahedron',
    'stell_Icosahedron', 'stell_Dodecahedron', 'stell_J12_TriangularDipyramid',
    'stell_J13_PentagonalDipyramid', 'stell_J17_GyroelongatedSquareDipyramid',
    'stell_J51_TriaugmentedTriangularPrism', 'stell_J84_SnubDisphenoid',
    'stell_RhombicDodecahedron', 'stell_RhombicIcosahedron', 'stell_RhombicTriacontahedron',
    'stell_RhombicHexecontahedron', 'stell_BilinskiDodecahedron',
    'stell_AcuteGoldenRhombohedron', 'stell_ObtuseGoldenRhombohedron']

    stellpolyseq=[]
    for el in stellpolyname:
        stellpolyseq.append(Sequence(os.path.join(el,el),12))
    if flags['mixupstell']:
        makeAnimation(stellpolyseq, "mixup_stell25.gif", param={"delay":"4", "resize":"50%"})

    # pingpong animation
    if flags['pingpong']:
        with click.progressbar(zip(basepolyseq,stellpolyseq)) as iterator:
            for el1,el2 in iterator:
                tpseq= pingpongseq(
                            (Sequence(os.path.join(el1,el1),12),
                             Sequence(os.path.join(el2,el2),12)),
                             nb=6)
                makeAnimation(tpseq, "mixup"+ el1 +".gif", param={"delay":"4", "resize":"50%"})

    # full length animation
    if flags['fullgif']:
        # polynames= basepolyname[:] + stellpolyname[:]
        # polynames= atltcolpoly
        polynames= temp2
        with click.progressbar(polynames) as names:
            for el in names:
                tpseq= [Sequence(os.path.join(el,el),-1)]
                makeAnimation(tpseq, el +".gif", param={"delay":"12", "resize":"100%"})

# make animated gif out of rendered frames organized in folders
# modify the animation parameters and the type of animation at your convenience
if __name__ == "__main__":
    flags= makeflags()
    # flags['mixupbase'] = True
    # flags['mixupstell'] = True
    # flags['pingpong'] = True
    flags['fullgif'] = True
    main(flags)
