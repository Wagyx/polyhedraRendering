# Run this script to generate a UV map and Image with 1 pixel per face so each face has a single colour.
# NOTE : For Texture paint you need to set Option/Bleed to not more than 2px - otherwise you'll get bleeding into unrelated faces

import bpy
import bmesh
import math

# Get selected/active object
obj = bpy.context.object
obj.update_from_editmode()
bpy.ops.object.mode_set(mode='EDIT')
bm = bmesh.from_edit_mesh(obj.data)

# Get count of faces
num_faces = len(bm.faces)

# Create image with 1 pixel per face (ie, WxH = <numfaces>x1)
image = bpy.data.images.new("facepixels_"+str(num_faces), width=num_faces*3, height=3)

# Create UV map
obj.data.uv_textures.new("facepixels_"+str(num_faces))
uv_layer = bm.loops.layers.uv[0]
bm.faces.layers.tex.verify()

# Process each face and position each loop vertex around the centre point
# of the central pixel of each 3x3 pixel group.
faceidx = -1
for f in bm.faces:
    faceidx = faceidx + 1
    loopidx = -1
    numverts = len(f.loops)
    for l in f.loops:
        loopidx = loopidx + 1
        luv = l[uv_layer]
        luv.uv[0] = (float(faceidx)+1.5+math.cos(float(loopidx)/numverts*math.pi*2+math.pi/4)/50)/num_faces
        luv.uv[1] = (0.0 + 0.5+math.sin(float(loopidx)/numverts*math.pi*2+math.pi/4)/30/3)