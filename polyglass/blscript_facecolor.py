# Run this script to generate a UV map and Image with 1 pixel per face so each face has a single colour.
# NOTE : For Texture paint you need to set Option/Bleed to not more than 2px - otherwise you'll get bleeding into unrelated faces

import bpy
import bmesh
import math

# Make UV map for the object
def create_uvmap(uvmapname, obj):
    bm = bmesh.from_edit_mesh(obj.data)

    obj.data.uv_textures.new(uvmapname)
    uv_layer = bm.loops.layers.uv[0]
    bm.faces.layers.tex.verify()

    # Get count of faces
    num_faces = len(bm.faces)

    # Process each face and position each loop vertex around the centre point
    # of the central pixel of each 3x3 pixel group.
    faceidx = -1
    for f in bm.faces:
        faceidx = faceidx + 1
        loopidx = -1
        numverts = len(f.loops)
        for l in f.loops:
            loopidx = loopidx + 1
            luv = l[uv_layer]
            luv.uv[0] = (float(faceidx)+0.5+math.cos(float(loopidx)/numverts*math.pi*2+math.pi/4)/50)/num_faces
            luv.uv[1] = (0.0 + 0.5+math.sin(float(loopidx)/numverts*math.pi*2+math.pi/4)/30/3)
    return

def set_pixel(img,x,y,color):
    width= image.size[0]
    offs = (x + int(y*width)) * 4
    for i in range(4):
        image.pixels[offs+i] = color[i]

def get_pixel(img,x,y):
    width = image.size[0]
    color=[]
    offs = (x + y*width) * 4
    for i in range(4):
        color.append( image.pixels[offs+i] )
    return color

# make an image from a color texture
def texture2image(imagename, texture):
    widtht= texture.size[0]
    image = bpy.data.images.new(imagename, width=width*3, height=3)
    yt=0
    for xt in range(0, width-1):
        color = get_pixel( texture, xt, yt)
        x= 1+xt*3
        y= 1
        set_pixel(image, x,int(y),color)
    return image


##############################################################################################
##############################################################################################

# Get selected/active object
obj = bpy.context.object
obj.update_from_editmode()
bpy.ops.object.mode_set(mode='EDIT')

# Create the uv map
uvmapname= "uvmap"
create_uvmap(uvmapname, obj)

# Create image with 1 pixel per face (ie, WxH = <numfaces>x1)
# from a color texture

#texturestr= 'stell_Dodecahedron_texture.png'
#texture = bpy.ops.image.open(texturestr)
#imagename= "uvimage"
#image= texture2image(imagename, texture)