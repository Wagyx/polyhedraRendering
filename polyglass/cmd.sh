cd Cube2/ && convert -delay 12 *_*.png -loop 0 res.gif && cd ..
cd Tetrahedron/ && convert -delay 12 *_*.png -loop 0 res.gif && cd ..
cd Dodecahedron/ && convert -delay 12 *_*.png -loop 0 res.gif && cd ..
cd Icosahedron/ && convert -delay 12 *_*.png -loop 0 res.gif && cd ..
cd Octahedron/ && convert -delay 12 *_*.png -loop 0 res.gif && cd ..
cd J12_TriangularDipyramid/ && convert -delay 12 *_*_*.png -loop 0 res.gif && cd ..
cd J13_PentagonalDipyramid/ && convert -delay 12 *_*_*.png -loop 0 res.gif && cd ..
cd J17_GyroelongatedSquareDipyramid/ && convert -delay 12 *_*_*.png -loop 0 res.gif && cd ..
cd J51_TriaugmentedTriangularPrism/ && convert -delay 12 *_*_*.png -loop 0 res.gif && cd ..
cd J84_SnubDisphenoid/ && convert -delay 12 *_*_*.png -loop 0 res.gif && cd ..
cd RhombicDodecahedron/ && convert -delay 12 *_*.png -loop 0 res.gif && cd ..
cd RhombicIcosahedron/ && convert -delay 12 *_*.png -loop 0 res.gif && cd ..
cd RhombicTriacontahedron/ && convert -delay 12 *_*.png -loop 0 res.gif && cd ..

cd stell_Cube2/ && convert -delay 12 *0*.png -loop 0 res.gif && cd ..
cd stell_Tetrahedron/ && convert -delay 12 *0*.png -loop 0 res.gif && cd ..
cd stell_Dodecahedron/ && convert -delay 12 *0*.png -loop 0 res.gif && cd ..
cd stell_Icosahedron/ && convert -delay 12 *0*.png -loop 0 res.gif && cd ..
cd stell_Octahedron/ && convert -delay 12 *0*.png -loop 0 res.gif && cd ..
cd stell_J12_TriangularDipyramid/ && convert -delay 12 *0*.png -loop 0 res.gif && cd ..
cd stell_J13_PentagonalDipyramid/ && convert -delay 12 *0*.png -loop 0 res.gif && cd ..
cd stell_J17_GyroelongatedSquareDipyramid/ && convert -delay 12 *0*.png -loop 0 res.gif && cd ..
cd stell_J51_TriaugmentedTriangularPrism/ && convert -delay 12 *0*.png -loop 0 res.gif && cd ..
cd stell_J84_SnubDisphenoid/ && convert -delay 12 *0*.png -loop 0 res.gif && cd ..
cd stell_RhombicDodecahedron/ && convert -delay 12 *0*.png -loop 0 res.gif && cd ..
cd stell_RhombicIcosahedron/ && convert -delay 12 *0*.png -loop 0 res.gif && cd ..
cd stell_RhombicTriacontahedron/ && convert -delay 12 *0*.png -loop 0 res.gif && cd ..

#cd / && convert -delay 12 *_*.png -loop 0 res.gif && cd ..
echo ‘fini’
