import bpy
import bmesh
import math
import os
import sys
import time
from collections import namedtuple

##########################################################################
##########################################################################
# printerror
def printerr(msg):
    return print(msg, file=sys.stderr)

# Make UV map for the object
def create_uvmap(uvmapname, obj):
    bm = bmesh.from_edit_mesh(obj.data)

    obj.data.uv_textures.new(uvmapname)
    uv_layer = bm.loops.layers.uv[0]
    bm.faces.layers.tex.verify()

    # Get count of faces
    num_faces = len(bm.faces)

    # Process each face and position each loop vertex around the centre point
    # of the central pixel of each 3x3 pixel group.
    faceidx = -1
    for f in bm.faces:
        faceidx = faceidx + 1
        loopidx = -1
        numverts = len(f.loops)
        for l in f.loops:
            loopidx = loopidx + 1
            luv = l[uv_layer]
            luv.uv[0] = (float(faceidx)+0.5+math.cos(float(loopidx)/numverts*math.pi*2+math.pi/4)/50)/num_faces
            luv.uv[1] = (0.0 + 0.5+math.sin(float(loopidx)/numverts*math.pi*2+math.pi/4)/30/3)
    return None

##########################################################################
##########################################################################

# sets the path depending on command line or in-Blender execution
COMMANDLINE = True

# find local path
dir_path = os.path.dirname(os.path.realpath(__file__))
dirpath,blendpath = os.path.split(dir_path)
if COMMANDLINE:
    dirpath=dir_path

printerr("--- START OF SCRIPT ---")
printerr(dirpath)

#models_path = "//data"
models_path= os.path.join(dirpath,"data")
render_path = "//render_out"

##########################################################################
##########################################################################
# sets the object to render and their properties here

# def props(name,scale_factor,height_delta):
#     return {"name":name, "scale":scale_factor, "height":height_delta}

Model = namedtuple('Joueur', ['name', 'scale', 'height'])

# Objects to render
objfilesstell=[]
# objfilesstell.append(Model("stell_Tetrahedron", 1.1, 0.0))
# objfilesstell.append(Model("stell_Cube2", 0.9, 0.0))
# objfilesstell.append(Model("stell_Octahedron", 1.1, 0.0))
# objfilesstell.append(Model("stell_Dodecahedron", 0.7, 0.0))
# objfilesstell.append(Model("stell_Icosahedron", 0.8, 0.0))
# objfilesstell.append(Model("stell_J12_TriangularDipyramid", 1.0, 0.0))
# objfilesstell.append(Model("stell_J13_PentagonalDipyramid", 1.0, 0.0))
# objfilesstell.append(Model("stell_J17_GyroelongatedSquareDipyramid", 0.9, 0.0))
# objfilesstell.append(Model("stell_J51_TriaugmentedTriangularPrism", 0.9, 0.0))
# objfilesstell.append(Model("stell_J84_SnubDisphenoid", 0.9, -0.7))
# objfilesstell.append(Model("stell_RhombicDodecahedron", 0.9, 0.0))
# objfilesstell.append(Model("stell_RhombicIcosahedron", 0.7, 0.0))
# objfilesstell.append(Model("stell_RhombicTriacontahedron", 0.6, 0.0))
# objfilesstell.append(Model("stell_RhombicHexecontahedron",0.6,0.0))
# objfilesstell.append(Model("stell_BilinskiDodecahedron",0.8,0.0))
# objfilesstell.append(Model("stell_AcuteGoldenRhombohedron",1.0,0.0))
# objfilesstell.append(Model("stell_ObtuseGoldenRhombohedron",1.0,0.0))

objfilesstell.append(Model("stell_Dodecahedron_sym", 0.7, 0.0))
objfilesstell.append(Model("stell_Icosahedron_5col", 0.8, 0.0))
objfilesstell.append(Model("stell_RhombicDodecahedron_5col", 0.9, 0.0))
# objfilesstell.append(Model("stell_RhombicIcosahedron", 0.7, 0.0))
objfilesstell.append(Model("stell_RhombicTriacontahedron_5col", 0.6, 0.0))
# objfilesstell.append(Model("stell_RhombicHexecontahedron",0.6,0.0))
objfilesstell.append(Model("stell_BilinskiDodecahedron_5col",0.8,0.0))


objfilesraw=[]
# objfilesraw.append(Model("Tetrahedron",2.0,0.0))
# objfilesraw.append(Model("Cube2",1.9,0.0))
# objfilesraw.append(Model("Octahedron",2.0,0.0))
# objfilesraw.append(Model("Dodecahedron",1.0,0.0))
# objfilesraw.append(Model("Icosahedron",1.6,0.0))
# objfilesraw.append(Model("J12_TriangularDipyramid",1.9,0.0))
# objfilesraw.append(Model("J13_PentagonalDipyramid",1.8,0.0))
# objfilesraw.append(Model("J17_GyroelongatedSquareDipyramid",1.3,0.0))
# objfilesraw.append(Model("J51_TriaugmentedTriangularPrism",1.7,0.0))
# objfilesraw.append(Model("J84_SnubDisphenoid",1.6,-1.3))
# objfilesraw.append(Model("RhombicDodecahedron",1.4,0.0))
# objfilesraw.append(Model("RhombicIcosahedron",1.0,0.0))
# objfilesraw.append(Model("RhombicTriacontahedron",0.9,0.0))
# objfilesraw.append(Model("RhombicHexecontahedron",0.6,0.0))
# objfilesraw.append(Model("BilinskiDodecahedron",1.0,0.0))
# objfilesraw.append(Model("AcuteGoldenRhombohedron",1.5,0.0))
# objfilesraw.append(Model("ObtuseGoldenRhombohedron",1.5,0.0))

# objfilesraw.append(Model("RhombicHexecontahedron5col",0.6,0.0))
# objfilesraw.append(Model("Dodecahedron6col",1.0,0.0))
# objfilesraw.append(Model("J12_TriangularDipyramid3col",1.9,0.0))
# objfilesraw.append(Model("J13_PentagonalDipyramid3col",1.8,0.0))
# objfilesraw.append(Model("J17_GyroelongatedSquareDipyramid3col",1.3,0.0))
# objfilesraw.append(Model("J51_TriaugmentedTriangularPrism3col",1.7,0.0))
# objfilesraw.append(Model("J84_SnubDisphenoid3col",1.6,-1.3))
# objfilesraw.append(Model("RhombicIcosahedron_alt",1.0,0.0))


objfiles=objfilesstell
objfiles.extend(objfilesraw)
#objfiles.append(Model(name,scale_factor,height_delta))

##########################################################################
##########################################################################
# Main work of looping over objects, assigning material properties and rendering them

#Load scene
scene = bpy.context.scene
renderimage= False
renderanimation= True
uvmapname= "uvmap"

#for each obj
totaltime= 0
for i,objfile in enumerate(objfiles):
    t0 = time.time()
    objroot= objfile.name
    msg = "item {} of {}: {}".format(i+1, len(objfiles), objroot)
    printerr(msg)

    # load ply object
    objfilename= objroot + '.ply'
    fname= os.path.join(models_path,objfilename)
    bpy.ops.import_mesh.ply(filepath=fname)
    obj = bpy.data.objects[objroot]

    scene.objects.active = obj
    obj.update_from_editmode()
    bpy.ops.object.mode_set(mode='EDIT')

    # move object and resize if necessary
    scalefactor= objfile.scale
    obj.location.z = 1.5 + objfile.height
    obj.scale.x = scalefactor
    obj.scale.y = scalefactor
    obj.scale.z = scalefactor

    # Create the uv map
    create_uvmap(uvmapname, obj)
    #myuvmap= obj.data.uv_textures[uvmapname]

    #load png texture
    imgname = objroot + '_texture.png'
    fname= os.path.join(models_path,imgname)
    texture= bpy.data.images.load(fname)

    #link object to material
    mymat= bpy.data.materials['Mymat']
    obj.data.materials.append(mymat)

    nodes= mymat.node_tree.nodes
    nodes["Image Texture"].image= texture
    nodes["UV Map"].uv_map= uvmapname

    # modifier
    bpy.ops.object.modifier_add(type='BEVEL')
    bpy.context.object.modifiers["Bevel"].width = 0.03
    bpy.context.object.modifiers["Bevel"].segments = 20

    # revert to object mode for next object
    bpy.ops.object.mode_set(mode='OBJECT')

    # render
    # one image
    if renderimage:
        # bpy.context.scene.render.filepath = os.path.join(render_path, objroot, objroot)
        bpy.context.scene.render.filepath = os.path.join(render_path, objroot)
        bpy.ops.render.render(write_still=True)

    # animation
    if renderanimation:
        bpy.context.scene.render.filepath = os.path.join(render_path, objroot, objroot+'_')
        bpy.ops.render.render(animation=True)

    #remove data
    scene.objects.unlink(obj)
    bpy.data.objects.remove(obj)
    tptime= time.time()-t0
    printerr('Done in {} seconds.'.format(tptime))
    totaltime += tptime
# loop end



printerr("--- DONE: Total time is {} seconds ---".format(totaltime))
