import bpy
import bmesh
import math
import os
import sys
import time
from collections import namedtuple

#########################################################
#########################################################
# printerror
def printerr(msg):
    return print(msg, file=sys.stderr)

#########################################################
#########################################################

# sets the path depending on command line or in-Blender execution
COMMANDLINE = True

# find local path
dir_path = os.path.dirname(os.path.realpath(__file__))
dirpath,blendpath = os.path.split(dir_path)
if COMMANDLINE:
    dirpath=dir_path

printerr("--- START OF SCRIPT ---")
printerr(dirpath)

#models_path = "//data"
models_path= os.path.join(dirpath,"data","mesh")
render_path = "//render_out"

#########################################################
#########################################################
# sets the object to render and their properties here

# Objects to render
objfiles= [entry.name for entry in os.scandir(models_path) if entry.name.endswith('.wrl') and entry.is_file()]

print(len(objfiles))
objfiles=sorted(objfiles)[:]
#########################################################
#########################################################
# Main work of looping over objects, assigning material properties and rendering them

#Load scene
scene = bpy.context.scene
renderimage= False
renderanimation= True

#for each obj
totaltime= 0
for i,objfilename in enumerate(objfiles):
    t0 = time.time()
    objroot,ext= os.path.splitext(objfilename)
    msg = "item {} of {}: {}".format(i+1, len(objfiles), objroot)
    printerr(msg)

    # load object
    fname= os.path.join(models_path,objfilename)
    #bpy.ops.import_mesh.obj(filepath=fname)
    #obj = bpy.data.objects[objroot]
    imported_object = bpy.ops.import_scene.x3d(filepath=fname)
    obj = bpy.context.selected_objects[0] ####<--Fix
    
    # mark edges to be traced
    for e in obj.data.edges:
        e.use_freestyle_mark = True
    obj.data.update() # is it necessary ?
    
    # go to edit mode to link things
    scene.objects.active = obj
    obj.update_from_editmode()
    bpy.ops.object.mode_set(mode='EDIT')

    #link object to material
    obj.data.materials.clear()
    mymat= bpy.data.materials['Mymat']
    obj.data.materials.append(mymat)
   
    # revert to object mode for next object
    bpy.ops.object.mode_set(mode='OBJECT')

    # RENDER
    # one image
    if renderimage:
        # bpy.context.scene.render.filepath = os.path.join(render_path, objroot, objroot)
        bpy.context.scene.render.filepath = os.path.join(render_path, objroot)
        bpy.ops.render.render(write_still=True)

    # animation
    if renderanimation:
        bpy.context.scene.render.filepath = os.path.join(render_path, objroot, objroot+'_')
        bpy.ops.render.render(animation=True)

    #remove data
    scene.objects.unlink(obj)
    bpy.data.objects.remove(obj)
    tptime= time.time()-t0
    printerr('Done in {} seconds.'.format(tptime))
    totaltime += tptime
# loop end

printerr("--- DONE: Total time is {} seconds ---".format(totaltime))
