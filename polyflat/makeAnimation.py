import sys
import os
import glob
from subprocess import call
from collections import namedtuple
import click

# name is the filename
# frame is the number of consecutive frames, if set to -1 all frames with filename are added
Sequence= namedtuple('Sequence', ['name','frame'])

def pingpongseq(seqlist,nb):
    sequences=[]
    for i in range(nb):
        sequences.extend(seqlist.copy())
    return sequences

def makeAnimation(sequences, outputfilename, param=None):
    if param is None:
        param={}
    if "resize" not in param.keys():
        param["resize"]= "100%"
    if "delay" not in param.keys():
        param["delay"]= "4"

    cwd = os.getcwd()
    cmdstr=[]
    count=0
    for el in sequences:
        elstr= os.path.join(cwd, el.name)
        # make list of files ending with _xxxx.png
        namelist= glob.glob(elstr + '_[0-9][0-9][0-9][0-9].png')
        namelist.sort()

        if el.frame == -1:
            st,ed=(0,len(namelist))
        else:
            st,ed= (count,count + el.frame)
            count= ed
        for fname in namelist[st:ed]:
            cmdstr.append(fname)

    cmdline= ["convert"]
    cmdline.extend(["-resize", param["resize"]])
    cmdline.extend(["-delay", param["delay"]])
    cmdline.extend(cmdstr)
    cmdline.extend(["-loop", "0"])
    cmdline.extend([outputfilename])
    call(cmdline)
    return None


def makeflags(defaultvalue=None):
    if defaultvalue is None:
        bval= False
    else:
        bval = defaultvalue
    return {'mixupbase': bval, 'mixupstell': bval, 'pingpong': bval, 'fullgif': bval}

def main(folderpath, flags):
    fname= folderpath[:-1]
    oname= os.path.join(folderpath, fname +".gif")
    tpseq= [Sequence(os.path.join(fname,fname),-1)]
    makeAnimation(tpseq, oname, param={"delay":"4", "resize":"100%"})

# make animated gif out of rendered frames organized in folders
# modify the animation parameters and the type of animation at your convenience
if __name__ == "__main__":
    flags= makeflags()
    # flags['mixupbase'] = True
    # flags['mixupstell'] = True
    # flags['pingpong'] = True
    flags['fullgif'] = True
    
    folderspath= sys.argv[1:]
    print(folderspath)
    with click.progressbar(folderspath) as it:
        for fpath in it:
            main(fpath, flags)
    
