import sys
import os
from subprocess import call
import click


def main(folderpath):

    param={"resize":"100%", "delay":"4"}
    fname= folderpath[:-1]
    oname= os.path.join(folderpath, fname +"_chiral.gif")

    cmdline= ["convert"]
    cmdline.extend(["-flop"])
    cmdline.extend(["-resize", param["resize"]])
    cmdline.extend(["-delay", param["delay"]])
    cmdline.extend([os.path.join(folderpath,"*.png")])
    cmdline.extend(["-loop", "0"])
    cmdline.extend([oname])
    call(cmdline)
    
    cmdline= ["mogrify"]
    cmdline.extend(["-reverse"])
    cmdline.extend([oname])
    call(cmdline)


def main2(filename):
    oname= filename[:-4]+"_chiral.png"
    cmdline= ["convert"]
    cmdline.extend(["-flop"])
    cmdline.extend([filename])
    cmdline.extend([oname])
    call(cmdline)
    
if __name__ == "__main__":

    folderspath= sys.argv[1:]
    print(folderspath)
    with click.progressbar(folderspath) as it:
        for fpath in it:
            main2(fpath)
